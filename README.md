# CustomFishing

CustomFishing is a plugin that allows for a chance to find fully customizable rewards while fishing. With infinite rewards and fully custom commands and messages, it will give players an amazing fishing experience.